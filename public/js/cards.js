window.addEventListener('load', () => {
  console.log('initialising cards');
});

class Cards {
  root = null; // div into which to put cards
  divCard = null;
  currentIdx = 0; // index of current card
  isMobile = false;
  constructor(contentId) {
    // rootId: the id of the div into which to put cards
    this.content = document.getElementById(contentId);
    document.addEventListener('keydown', event => {
      if (event.key == 'Escape') {
        window.location.replace('../');
      }
      if (this.isFront && ['ArrowLeft', 'ArrowRight', 'ArrowUp', 'ArrowDown'].includes(event.key)) {
        // Any key flips the card
        this._show.bind(this)();
        return;
      }
      if (!this.isFront) {
        if (event.key == 'ArrowLeft') {
          this._chooseNo.bind(this)();
          return;
        }
        if (event.key == 'ArrowRight') {
          this._chooseYes.bind(this)();
          return;
        }
      }
    }); // keydown
    this._detectMobile();
  }

  generateCards(fn) {
    // supply a function here which returns cards, e.g.
    // [
    //  { front: [ 0 ], back: 'C', },
    //  { front: [ 2 ], back: 'D', }
    // ];
    this.cards = fn();
  }


  setDrawFront(fn) {
    this._drawFront = fn;
  }
  setDrawBack(fn) {
    this._drawBack = fn;
  }


  // PRIVATE
  drawCard() {
    let content = this.content;
    content.innerHTML = '';
    let divCard = this._mk('div');
    divCard.id = 'card-content';
    this.divCard = divCard;
    content.appendChild(divCard);
    let divMenu = this._mk('div');
    divMenu.id = 'card-menu';
    divMenu.style = 'background: aliceblue';
    content.appendChild(divMenu);
    let idx = this.currentIdx;
    if (this.isFront) {
      this.drawFront();
    } else {
      this.drawBack();
    }
    // Menu buttons
    if (this.isFront) {
      // Draw show button
      let btn = this._mk('button');
      btn.className = 'btn-show';
      btn.innerHTML = 'show';
      divMenu.appendChild(btn);
      btn.addEventListener('click', this._show.bind(this));
    } else {
      // Draw yes/no button
      // btn: no
      let btnN = this._mk('button');
      btnN.className = 'btn';
      btnN.id = 'btn-no';
      btnN.innerHTML = 'no';
      divMenu.appendChild(btnN);
      btnN.addEventListener('click', this._chooseNo.bind(this));
      // btn: yes
      let btnY = this._mk('button');
      btnY.className = 'btn';
      btnY.innerHTML = 'yes';
      btnY.id = 'btn-yes';
      divMenu.appendChild(btnY);
      btnY.addEventListener('click', this._chooseYes.bind(this));
    }
  } // drawCard

  drawFront() {
    let curCard = this.cards[this.currentIdx].front;
    this._drawFront(curCard);
  }


  drawBack() {
    let curCard = this.cards[this.currentIdx].back;
    this._drawBack(curCard);
  }


  run() {
    this.isFront = true;
    const idx = this._getRandomCardId();
    this.currentIdx = idx;
    this.drawCard();
  }


  testPrintCards() {
    console.log('cards:', this.cards);
  }

  _getRandomCardId() {
    return this._getRandomInt(this.cards.length);
  }
  _getRandomInt(max) {
    return Math.floor(Math.random() * max);
  }


  _mk(name) {
    return document.createElement(name);
  }


  _show() {
    this.isFront = !this.isFront;
    this.drawCard();
  }
  _chooseYes(e) {
    let btn = document.getElementById('btn-yes');
    btn.style.background = 'blue';
    this.currentIdx = this._getRandomCardId();
    this.isFront = !this.isFront;
    this.drawCard();
  }
  _chooseNo(e) {
    let btn = document.getElementById('btn-no');
    btn.style.background = 'blue';
    this.currentIdx = this._getRandomCardId();
    this.isFront = !this.isFront;
    this.drawCard();
  }


  _detectMobile() {
    // do this on window 'resize' event
    if (navigator.userAgent.includes('Mobile')
      || 'ontouchstart' in document.documentElement) {
      this.isMobile = true;
      document.body.style.fontSize = '3rem';
    } else {
      this.isMobile = false;
      console.log('not mobile');
    }
  }
}
