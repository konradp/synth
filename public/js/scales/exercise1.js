// natural minor
pd = null;
window.addEventListener('load', () => {
  let cards = new Cards('card-root');

  cards.generateCards(() => {
    // this is the return array that we will build, e.g.
    // [
    //  { front: [ 0, 2, 4, 5, 7, 9, 11 ], back: 'C maj', },
    //  { front: [ 0, 2, 3, 5, 7, 8, 10 ], back: 'C min', }
    // ];
    let _cards = [];
    const countKeys = 36;
    const notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
    const maj = [ 0, 2, 4, 5, 7, 9, 11 ];
    const min = [ 0, 2, 3, 5, 7, 8, 10 ]
    for (let i = 0; i<12; i++) {
      let rootName = notes[i%12];
      let noteOffset = i;
      let newMaj = maj.map(e => e+i);
      let newMin = min.map(e => e+i);
      if (Math.max(...newMaj) < countKeys) {
        _cards.push({
          front: newMaj,
          back: {
            text: `${rootName} maj`,
            keys: newMaj,
          }
        });
      }
      if (Math.max(...newMin) < countKeys) {
        _cards.push({
          front: newMin,
          back: {
            text: `${rootName} min`,
            keys: newMin,
          }
        });
      }
    }
    return _cards;
  }); // generateCards

  cards.setDrawFront(function f(card) {
    let div = this.divCard;
    div.style = 'font-size: 4em';
    let canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    div.appendChild(canvas);
    let pd = new PianoDraw('canvas');
    pd.setKeys(card);
  });
  cards.setDrawBack(function f(card) {
    let div = this.divCard;
    div.style = 'font-size: 4em';
    let canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    div.appendChild(canvas);
    let pd = new PianoDraw('canvas');
    pd.setKeys(card.keys);
    let text = document.createElement('div');
    text.innerHTML = card.text;
    div.appendChild(text);
  });

  cards.run();
});
