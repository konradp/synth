let pd1 = null;
const maj1 = [ 0, 2, 4, 5, 7, 9, 11, 12 ];
const min1 = [ 0, 2, 3, 5, 7, 8, 10, 12 ]
const notes1 = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
let tonicOffset1 = 0;
window.addEventListener('load', () => {
  //let pd = document.getElementsById('ex1');
  pd1 = new PianoDraw('ex2', settings={ countOctaves:2} );
  pd1.countOctaves = 2;
  pd1.drawKeyboard();
  drawScale1();
  _get('btn-prev1').addEventListener('click', () => nextPrevScale1(-1));
  _get('btn-next1').addEventListener('click', () => nextPrevScale1(1));
  _get('scale-type1').addEventListener('change', drawScale1);
});


function nextPrevScale1(increment) {
  tonicOffset1 = (tonicOffset1 + increment).mod(12);
  drawScale1();
}


function drawScale1() {
  // Get the tonicOffset
  //let tonic = notes[tonicOffset];
  _get('tonic1').innerHTML = notes1[tonicOffset1];
  let scaleType = _get('scale-type1').value;
  let scale = [];
  if (scaleType == 'major') {
    scale = [...maj1];
  } else {
    scale = [...min1];
  }
  scale = scale.map(e => e + tonicOffset1);
  scale[0] = { key: scale[0], color: 'blue' };
  scale[scale.length-1] = { key: scale[scale.length-1], color: 'blue' };
  pd1.setKeys([]);
  pd1.setKeys(scale);
}


Number.prototype.mod = function(n) {
  // https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
  "use strict";
  return ((this % n) + n) % n;
};


function _get(id) {
  return document.getElementById(id);
}
