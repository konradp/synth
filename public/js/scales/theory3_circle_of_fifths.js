let pd = null;
const maj = [ 0, 2, 4, 5, 7, 9, 11, 12 ];
const min = [ 0, 2, 3, 5, 7, 8, 10, 12 ]
const notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
let tonicOffset = 0;
let countKeys = 0; // 36
let circle = [



];


window.addEventListener('load', () => {
  //let pd = document.getElementsById('ex1');
  let canvas = document.getElementById('circle');











  pd = new PianoDraw('ex1', settings={ countOctaves:2} );
  pd.countOctaves = 2;
  countKeys = pd.countKeys;
  pd.drawKeyboard();
  drawScale();
  _get('btn-prev').addEventListener('click', () => nextPrevScale(-1));
  _get('btn-next').addEventListener('click', () => nextPrevScale(1));
  _get('scale-type').addEventListener('change', drawScale);
  document.addEventListener('keydown', event => {
    switch (event.key) {
      case 'ArrowLeft':
        nextPrevScale(-1); break;
      case 'ArrowRight':
        nextPrevScale(1); break;
      default: break;
    }
  });
});


function nextPrevScale(increment) {
  tonicOffset = (tonicOffset + increment).mod(12);
  drawScale();
}


function drawScale() {
  // Get the tonicOffset
  //let tonic = notes[tonicOffset];
  _get('tonic').innerHTML = notes[tonicOffset];
  let scaleType = _get('scale-type').value;
  let scale = [];
  if (scaleType == 'major') {
    scale = [...maj];
  } else {
    scale = [...min];
  }
  scale = scale.map(e => (e + tonicOffset)%12);
  //scale[0] = { key: scale[0], color: 'blue' };
  //scale[scale.length-1] = { key: scale[scale.length-1], color: 'blue' };
  let newScale = [];
  for (let i = 0; i < countKeys; i++) {
    if (scale.includes(i%12)) {
      if (i%12 == scale[0]) {
        newScale.push({key: i, color: 'blue'});
      } else {
        newScale.push(i);
      }
    }
  }
  pd.setKeys([]);
  pd.setKeys(newScale);
}


Number.prototype.mod = function(n) {
  // https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
  "use strict";
  return ((this % n) + n) % n;
};


function _get(id) {
  return document.getElementById(id);
}
