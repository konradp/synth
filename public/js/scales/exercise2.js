// show relative minor for scale
pd = null;
window.addEventListener('load', () => {
  let cards = new Cards('card-root');

  cards.generateCards(() => {
    // this is the return array that we will build, e.g.
    // [
    //  { front: [ 0, 2, 4, 5, 7, 9, 11 ], back: 'C maj', },
    //  { front: [ 0, 2, 3, 5, 7, 8, 10 ], back: 'C min', }
    // ];
    let _cards = [];
    const countKeys = 36;
    const notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
    const maj = [ 0, 2, 4, 5, 7, 9, 11 ];
    const min = [ 0, 2, 3, 5, 7, 8, 10 ]
    for (let i = 0; i<12; i++) {
      let rootName = notes[i%12];
      let noteOffset = i;
      function offsetKeys(e) {
        if (Number.isInteger(e)) {
          return e + i;
        } else {
          return {
            ...e,
            key: e.key + i,
          };
        }
      }
      let newMaj = maj.map(offsetKeys);
      let newMin = min.map(offsetKeys);
      let frontMaj = maj.map(e => e+i);
      let frontMin = min.map(e => e+i);
      let backMaj = [...frontMaj];
      let backMin = [...frontMin];
      backMaj[5] = { key: backMaj[5], color: 'red' };
      backMin[2] = { key: backMin[2], color: 'red' };
      let majRelativeMin = notes[frontMaj[5]%12];
      let minRelativeMaj = notes[frontMin[2]%12];
      if (Math.max(...frontMaj) < countKeys) {
        _cards.push({
          front: frontMaj,
          back: {
            key: `${rootName} maj <br> ${majRelativeMin} min`,
            notes: backMaj,
          }
        });
      }
      if (Math.max(...frontMin) < countKeys) {
        _cards.push({
          front: frontMin,
          back: {
            key: `${rootName} min <br> ${minRelativeMaj} maj`,
            notes: backMin,
          }
        });
      }
    }
    return _cards;
  }); // generateCards

  cards.setDrawFront(function f(card) {
    let div = this.divCard;
    let canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    div.appendChild(canvas);
    let pd = new PianoDraw('canvas');
    pd.setKeys(card);
  });
  cards.setDrawBack(function f(card) {
    let div = this.divCard;
    div.style = 'font-size: 4em';
    // canvas
    let canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    div.appendChild(canvas);
    let br = document.createElement('br');
    div.appendChild(br);
    let pd = new PianoDraw('canvas');
    pd.setKeys(card.notes);
    // text
    let text = document.createElement('div');
    text.innerHTML = card.key;
    div.appendChild(text);
  });

  cards.run();
});
