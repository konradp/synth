let isMobile = false;

window.addEventListener('load', () => {
  detectMobile();

  // Font size
  if (isMobile) {
    let body = document.querySelector('body');
    body.style.fontSize = '3rem';
  }
});


function getCssRule(selector) {
  // Example:
  //   divRule = getCssRule('div');
  //   if (divRule) {
  //     divRule.style.whiteSpace = 'nowrap';
  //  }
  //  Loop through all stylesheets
  for (var i = 0; i < document.styleSheets.length; i++) {
    var stylesheet = document.styleSheets[i];
    // Loop through all rules in the stylesheet
    for (var j = 0; j < stylesheet.cssRules.length; j++) {
      var rule = stylesheet.cssRules[j];
      // Check if the selector matches
      if (rule.selectorText === selector) {
        return rule;
      }
    }
  }
  return null;
}

function detectMobile() {
  // do this on window 'resize' event
  if (navigator.userAgent.includes('Mobile')
    || 'ontouchstart' in document.documentElement) {
    this.isMobile = true;
    document.body.style.fontSize = '3rem';
  } else {
    this.isMobile = false;
    console.log('not mobile');
  }
}

