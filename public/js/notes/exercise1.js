pd = null;
window.addEventListener('load', () => {
  let cards = new Cards('card-root');

  cards.generateCards(() => {
    // this is the return array that we will build, e.g.
    // [
    //  { front: [ 0 ], back: 'C', },
    //  { front: [ 2 ], back: 'D', }
    // ];
    let _cards = [];
    const countKeys = 36;
    const notes = ['C', 'C#, Db', 'D', 'D#, Eb', 'E', 'F', 'F#, Gb',
      'G', 'G#, Ab', 'A', 'A#, Bb', 'B' ];
    for (let i = 0; i<countKeys; i++) {
      let noteName = notes[i%12];
      let noteKey = i;
      _cards.push({
        front: [ noteKey ],
        back: noteName,
      });
    }
    return _cards;
  }); // generateCards

  cards.setDrawFront(function f(card) {
    let div = this.divCard;
    let canvas = document.createElement('canvas');
    canvas.id = 'canvas';
    canvas.style.width = '100%';
    div.appendChild(canvas);
    let pd = new PianoDraw('canvas');
    pd.setKeys(card);
  });
  cards.setDrawBack(function f(card) {
    let div = this.divCard;
    div.style = 'font-size: 4em';
    div.innerHTML = card;
  });

  cards.run();
});
