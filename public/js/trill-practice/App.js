// trill-practice
// Adapted from
// https://developer.mozilla.org/en-US/docs/Web/API/Web_Audio_API/Simple_synth
// https://developer.mozilla.org/en-US/docs/Web/API/Web_MIDI_API
// TODO: See this: https://webaudio.github.io/web-midi-api/#a-simple-monophonic-sine-wave-midi-synthesizer

let midi = null;  // global MIDIAccess object
let synth = null;
let debug = false;
let midiInitialised = false;
let chart = {};
let keysPressed = 0;


function onMIDISuccess(midiAccess) {
  // MIDI has been initialised
  console.log('MIDI initialised');
  midi = midiAccess;
  uiSetMidiDevicesList();
  startLoggingMIDIInput(midi);
  synth = new Synth();
}


function onMIDIFailure(msg) {
  console.log("Failed to get MIDI access - " + msg);
}


navigator.requestMIDIAccess()
  // Device listener
  .then(function(access) {
    // Get lists of available MIDI controllers
    const inputs = access.inputs.values();
    const outputs = access.outputs.values();
    access.onstatechange = function(e) {
      // Print information about the (dis)connected MIDI controller
      uiSetMidiDevicesList();
      console.log('New device:', {
        'e.port.name': e.port.name,
        'e.port.manufacturer': e.port.manufacturer,
        'e.port.state': e.port.state
      });
    };
  });


function graphAppend(velocity) {
  chart.data.datasets[0].data.push(velocity);
  keys = chart.data.datasets[0].data;
  chart.data.labels.push('b');
  console.log(keys.slice(-2));
  let keys2 = keys.slice(-2);
  let avg2 = (keys2.reduce((a, b) => a + b, 0)/keys2.length) || 0;
  let keys10 = keys.slice(-10);
  let avg10 = (keys10.reduce((a, b) => a + b, 0)/keys10.length) || 0;
  chart.data.datasets[1].data.push(avg2);
  chart.data.datasets[2].data.push(avg10);
  chart.update();
}


function onMIDIMessage(event) {
  // Event types:
  // - 144: note on
  // - 128: note off
  let eve = event.data[0];
  let key = event.data[1];
  let vel = event.data[2];
  if (debug) {
    console.log('EVENT', eve, key, vel);
  }
  if (eve == 144) {
    synth.midiNoteOn(key, vel);
    // Graph update
    graphAppend(vel);
  } else if (eve == 128) {
    synth.midiNoteOff(key);
  }
}


function startLoggingMIDIInput(midiAccess, indexOfPort) {
  midiAccess.inputs.forEach( function(entry) {entry.onmidimessage = onMIDIMessage;});
}


function uiSetMidiDevicesList() {
  var midi_devices = document.querySelector('#midi-devices');
  midi_devices.innerHTML = '';
  var i = 0;
  for (var entry of midi.inputs) {
    i++;
    var input = entry[1];
    var option = document.createElement('option');
    //option.value = input.id;
    option.innerHTML = input.name;
    option.value = input.id;
    if (i == midi.inputs.size) {
      option.selected = true;
    }
    midi_devices.appendChild(option);
  }
}


function changeVolume(ev) {
  synth.setGain(this.value);
  console.log('vol', ev, this.value);
}


function drawChart() {
  const chartLabels = [];
  const chartData = {
    labels: chartLabels,
    datasets: [{
      label: 'keys',
      backgroundColor: 'rgb(255, 99, 132)',
      borderColor: 'rgb(255, 99, 132)',
      data: [],
    }, {
      label: 'avg2', borderColor: 'red',
      data: [],
    }, {
      label: 'avg10', borderColor: 'blue',
      data: [],
    }]
  };
  const config = {
    type: 'line',
    data: chartData,
    options: {
      animation: false,
      elements: {
        point: { radius: 0 },
      },
    },
  };
  chart = new Chart(
    document.getElementById('myChart'),
    config
  );
}


function isWhite(i) {
    let keysWhite = [0,2,4,5,7,9,11];
    if (keysWhite.includes((i) % 12)) {
      return true;
    }
    return false;
}


function drawKey(i, highlight=false) {
  var canvas = document.getElementById('keyboard');
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = 'black';
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 2;
    let width = 12;
    let height = 0;
    let x = 0;
    let keysWhite = [0,2,4,5,7,9,11];
    let j = i-1;
    //console.log('i%12', i%12);
    if (highlight) {
      ctx.fillStyle = 'purple';
      ctx.strokeStyle = 'black';
    }
    if (isWhite(j)) {
      // white key
      height = 70;
      x = (keysWhite.indexOf(j%12)*width) + Math.floor(j/12)*7*width;
      if (highlight) {
        ctx.fillRect(x, 0, width, height);
        //drawKeyboard();
      } else {
        ctx.strokeRect(x, 0, width, height);
      }
    } else {
      // black key
      height = 40;
      x = ((width*7)/12)*(i%12-1) + Math.floor(i/12)*width*7;
      width = 8;
      ctx.fillRect(x, 0, width, height);
    }
  }
}


function drawKeyboard() {
  var canvas = document.getElementById('keyboard');
  if (canvas.getContext) {
    var ctx = canvas.getContext('2d');
    for (let i = 1; i<=88; i++) {
      drawKey(i);
    }
  }
}


// MAIN
function Run() {
  document.body.addEventListener('click', () => {
    if (!midiInitialised) {
      navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
      volCtl = document.querySelector("input[name='volume']");
      volCtl.addEventListener('input', changeVolume, false);
    }
    midiInitialised = true;
  });
  drawChart();
  drawKeyboard();
}
