// TODO:
// - icons:
//   - flashcards: three cards, like in card games
//   - theory: book
//   - midi controller exercise: piano keys
//   - listening exercise: headphones
let isMobile = false;


window.addEventListener('load', () => {
  console.log('scales-practice');
  const menu = {
    notes: [
      {
        title: 'exercise 1',
        sub: 'note names',
        type: 'cards',
      }, {
        title: 'exercise 2',
        sub: 'note names by ear',
        type: 'cards',
        wip: true,
      }, {
        title: 'exercise 3',
        sub: 'Q: show + play note sound (within scale, single octave), A: play note',
        type: 'midi',
        wip: true,
      }, {
        title: 'exercise 4',
        sub: 'Q: play note sound (within scale, single octave) A: play note',
        type: 'midi',
        wip: true,
      }, {
        title: 'exercise 5',
        sub: 'Q: play note sound (within scale, multiple octaves) A: play note',
        type: 'midi',
        wip: true,
      }
    ],
    intervals: [
      {
        title: 'theory 1',
        type: 'theory',
        sub: 'todo',
        wip: true,
      },
    ],
    scales: [
      {
        title: 'theory 1',
        type: 'theory',
        sub: 'major and minor',
      }, {
        title: 'exercise 1',
        sub: 'basic scales',
        type: 'cards',
        //notes: '''
        //    - scale intervals: 2, 2, 1, ... <- major
        //    - minor scale: intervals
        //    - major vs minor (natural)
        //    - minor: harmonic, melodic
        //  - guess:
        //    - minor or major
        //    - key: C, D, E, ...
        //  - learning settings:
        //    - minor or major only
        //    - specific keys only
      }, {
        title: 'theory 2',
        type: 'theory',
        sub: 'relative minor',
      }, {
        title: 'theory 3',
        type: 'theory',
        sub: 'circle of fifths',
      }, {
        title: 'exercise 2',
        sub: 'relative minor',
        type: 'cards',
        //  - guess:
        //    - minor or major
        //    - key: C, D, E, ...
        //    - relative key: major -> minor, minor -> major
      },{
        title: 'exercise 3',
        sub: '+whole keyboard, no starting point',
        type: 'cards',
        wip: true,
        //  - two outcome pictures here
        //  - guess:
        //    - minor or major
        //    - key: C, D, E, ...
        //    - relative key/scale
      }, {
        title: 'exercise 4',
        sub: 'melodic minor',
        type: 'cards',
        wip: true,
      }, {
        title: 'exercise 5',
        sub: 'natural minor',
        type: 'cards',
        wip: true,
      }
    ], // end scales
    other: [
      {
        title: 'trill practice',
        sub: 'Playing trills evenly',
        type: 'midi',
      }
    ],
  };

  let root = _get('.content');
  let i = 0;
  for (let sectionTitle in menu) {
    // for each section
    let divSection = _mk('div');
    divSection.className = 'section';
    root.appendChild(divSection)
      .appendChild(_mk('h2'))
      .innerHTML = sectionTitle;
    for (item of menu[sectionTitle]) {
      // item
      i++;
      let divItem = _mk('a');
      divItem.className = 'item';
      divItem.href = `./${sectionTitle}/${item.title.split(' ').join('')}`
      if (item.wip) {
        divItem.style.background = 'silver';
        divItem.href = '';
      }
      // img
      let img = _mk('img');
      img.className = 'item-img';
      img.src = `./img/${item.type}.svg`;
      divItem.appendChild(img);
      // title
      //let divItemTitle = _mk('div');
      //divItemTitle.className = 'item-title';
      ////divItemTitle.innerHTML = item.title;
      //divItemTitle.innerHTML = i;
      //divItem.appendChild(divItemTitle);
      // sub
      let divItemSub = _mk('div');
      divItemSub.className = 'item-sub';
      divItemSub.innerHTML = item.sub;
      divItem.appendChild(divItemSub);
      //
      divSection.appendChild(divItem);
    }
  }
  detectMobile();
});


// helpers
function _mk(name) {
  return document.createElement(name);
}
function _get(name) {
  return document.querySelector(name);
}


function detectMobile() {
  // do this on window 'resize' event
  if (navigator.userAgent.includes('Mobile') 
    || 'ontouchstart' in document.documentElement) {
    isMobile = true;
    document.body.style.fontSize = '3rem';
  } else {
    isMobile = false;
    console.log('not mobile');
  }
}
