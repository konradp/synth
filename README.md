# piano-practice
<https://konradp.gitlab.io/piano-practice>

## What it does
For now, this tool is to practice trills on piano. It draws a chart over time, showing the note velocity.

- connect a MIDI controller
- repeatedly play two notes
- the graph will show note velocity
- notes will be played by a basic sine wave synth

## Run
```BASH
npm install
npm start
```
Navigate in web browser to http://localhost:3000/

## TODO: Piano sounds
Download sound pack from  
https://freesound.org/people/feelander/packs/17718/
