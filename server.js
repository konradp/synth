const express = require('express');
const app = express();
const port = 3000;

// Serve static pages
app.use(express.static('public', {
  extensions: ['html']
}))

// Start server
app.listen(port, () => {
  console.log(`Listening on ${port}`)
})
